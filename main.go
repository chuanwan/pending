package main

import (
	"log"
	"time"
)

func main() {
	log.Println("pending entry.")
	tick := time.NewTicker(time.Second)
	for range tick.C {
		log.Printf("pending...\n")
	}
}
